<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AB-Com
 */

get_header(); 

if ( is_home() && ! is_front_page() ) : ?>
<div class="blog-posts__block">
    <div class="wrapper">
        <div class="recent-posts__wrap flex-block article-wrap">

                <?php
            $args = array(
                'posts_per_page'  => 9,
                'cat'      => pll_get_term(1)
              );
              $posts = get_posts($args);
              if( $posts ){ ?>

                <?php foreach( $posts as $post ): ?>
                    <div class="three-col__item">
                        <?php get_template_part( 'template-parts/blog-item', get_post_format() ); ?>
                    </div>
                <?php endforeach; 
                wp_reset_postdata();
               } else {
                get_template_part( 'template-parts/content', 'none' );
              }
            ?>
        </div>

        <script>
            var true_posts = '<?php echo serialize($args); ?>';
            var count = +'<?php echo $args["posts_per_page"]; ?>';
            <?php $count_posts = wp_count_posts(); ?>
            var max_post = <?php echo $published_posts = get_category($args["cat"])->category_count; ?>;


              // ajax load more
            document.addEventListener('DOMContentLoaded', function(){
              $('.js-load-btn').click(function(){
                var showNext = ($(window).width() < 960) ? 3 : 9;
                var data = {
                  'action': 'loadmore',
                  'query': true_posts,
                  'count' : count,
                  'showNext' : showNext
                };
                // var button_text = $(this).text();
                // $(this).find('a').text('Загрузка...');     
                $.ajax({
                  url:'/wp-admin/admin-ajax.php', 
                  data:data,
                  type:'POST', 
                  success:function(data){
                    // $("#true_loadmore a").text(button_text);
                    if( data ) { 
                      $(".article-wrap").append(data);
                      if (max_post <= count+showNext) $('.bottom-btn-wrap').remove();
                      count += showNext; 
                    } else {
                      $('.bottom-btn-wrap').remove();
                    }
                  }
                });
                return false;
              });
            });              

        </script>
        <div class="center-wrap bottom-btn-wrap">
            <script>
                if (max_post > count) {
                    var btn = document.createElement('a');
                    btn.setAttribute("href", "#");
                    btn.className = "reg-btn js-load-btn";
                    btn.innerHTML = "<?php echo pll_e('showmore')?>";
                    document.querySelector('.bottom-btn-wrap').appendChild(btn);
                }
            </script>
        </div>
    </div>
</div>
<?php
endif;
get_footer();
