<?php $main_page_id = get_option( 'page_on_front' ); ?>

<div class="manager__block manager__block_absolute flex-block">
    <div class="manager__icon">
        <div class="manager__img"><img src="<?php echo get_template_directory_uri(); ?>/images/manager-img.jpg" alt=""></div>
        <div class="manager__title"><?php the_field('title', $main_page_id); ?></div>
    </div>
    <div class="manager__body">
        <div class="manager__text"><?php the_field('text', $main_page_id); ?></div>
        <div class="manager__btn"><a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php the_field('button', $main_page_id); ?></a></div>
    </div>
</div>