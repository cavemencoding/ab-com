<?php
/**
 * Template Name: About us Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
    	<div class="main-title main-title_smaller main-title_about"><?php the_field('strong_title'); ?></div>
        <div class="content__block textmarkup__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>
<?php if( have_rows('block_1') ): ?>
	<div class="about-years__block bg_lt-blue">
		<div class="wrapper">
			<div class="about-years__wrap flex-block">
				<?php while ( have_rows('block_1') ) : the_row(); ?>
					<div class="four-col__item">
						<div class="about-years__item center-wrap">
							<div class="about-years__icon flex-block flex-ai-c flex-jc-c"><img src="<?php the_sub_field('icon'); ?>" alt=""></div>
							<div class="about-years__title"><?php the_sub_field('title'); ?></div>
							<div class="about-years__text"><?php the_sub_field('text'); ?></div>
						</div>
					</div>
				<?php  endwhile; ?>
			</div>
		</div>
	</div>
<?php endif; ?>  
   
<?php
get_footer();
