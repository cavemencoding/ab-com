<?php
/**
 * Template Name: Service Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>
 <?php if( have_rows('block_1') ): ?>

<div class="info-feats__block bg_lt-blue">
    <div class="wrapper wrapper_860">
      <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_1_-_title'); ?></div>
      </div> 
      <div class="info-feats__wrap">
<?php while ( have_rows('block_1') ) : the_row();
$i++; ?>
<?php 
if ($i == 1 || $i == 3 || $i == 6) {
  echo '<div class="flex-block">';

}
if ( $i >= 3 && $i <= 5) {
  echo '<div class="three-col__item">';
} else {
  echo '<div class="two-col__item">';

}
?>
          
            <a href="#" class="js-modal-link info-feats__item center-wrap" modal-target="services-modal<?php echo $i; ?>">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="info-feats__item-title"><?php the_sub_field('title'); ?></div>
            </a>
            <div style="display:none" class="hidden">
              <div class="services-modal<?php echo $i; ?>">
                  <div class="box-modal">
                    <button class="box-modal_close arcticmodal-close"></button>
                    <div class="modal-header">
                        <span class="modal-title"><?php the_sub_field('title'); ?></span>
                    </div>
                    <div class="modal-body">
                      <div class="services-modal-text content__block"><?php the_sub_field('text'); ?></div>
                    </div>
                    <div class="modal-footer">
                        <a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php echo pll_e('requestform')?></a>
                    </div>
                  </div>
              </div>
          </div>
          </div>
          <?php 
if ($i == 2 || $i == 5) {
  echo '</div>';

}
?>
<?php  endwhile; ?>
         
      </div>
      </div>
    </div>
    <div class="center-wrap bottom-btn-wrap"><a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php echo pll_e('requestform')?></a></div>
    </div>
</div>
<?php endif; ?>  
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
