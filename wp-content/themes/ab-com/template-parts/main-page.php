<?php
/**
 * Template Name: Main Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


   <?php if( have_rows('service_block') ): ?>
        <div class="front-service__block">
            <div class="wrapper">
                <div class="front-service__wrap flex-block">

            <?php while ( have_rows('service_block') ) : the_row(); ?>
                    <div class="two-col__item">
                        <?php $post_id = get_sub_field('link', false, false); ?>
                        <a href="<?php the_sub_field('link'); ?>" class="front-service__item">
                            <img src="<?php the_sub_field('image'); ?>" alt="">
                            <div class="front-service__item-inner front-service__item-inner1 flex-block flex-jc-c flex-ai-c">
                                <div class="front-service__title"><?php echo get_the_title($post_id); ?></div>
                            </div>
                            <div class="front-service__item-inner front-service__item-inner2 flex-block flex-jc-c flex-ai-c">
                                <div class="front-service__title"><?php echo get_the_title($post_id); ?></div>
                            </div>
                        </a>
                    </div>


               <?php  endwhile; ?>

                </div>
            </div>
            <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
        </div>

    <?php endif; ?> 
    <?php if( get_field('enable_benefits_block') ): ?>
        
        <div class="benefits__block">
            <div class="wrapper">
                <div class="main-title-wrap">
                    <div class="main-title color_wht"><?php the_field('benefits_block_-_titlte'); ?></div>
                </div>
                <div class="benefits__wrap flex-block flex-jc-sb">

       <?php if( have_rows('benefits_block') ): ?>

                <?php while ( have_rows('benefits_block') ) : the_row(); ?>
                    <div class="benefits__item center-wrap color_wht">
                        <div class="benefits__icon"><img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>"></div>
                        <div class="benefits__num"><?php the_sub_field('title'); ?></div>
                        <div class="benefits__title"><?php the_sub_field('text'); ?></div>
                    </div>

                   <?php  endwhile; ?>

        <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>    

    <div class="front-geo__block bg_lt-blue">
        <div class="wrapper">
            <div class="main-title-wrap">
                <div class="main-title"><?php the_field('geography_block_-_titlte'); ?></div>
            </div> 
          <?php get_template_part( 'template-parts/geo-block', get_post_format() ); ?>
        </div>
    </div>
        
       <?php if( have_rows('recent_posts_block') ): ?>
        <div class="recent-posts__block">
            <div class="wrapper">
                <div class="main-title-wrap">
                    <div class="main-title"><?php the_field('recent_posts_block_-_titlte'); ?></div>
                </div>  
                <div class="recent-posts__wrap flex-block">
                <?php while ( have_rows('recent_posts_block') ) : the_row(); ?>
                    <?php $post_id = get_sub_field('page_link', false, false); ?>

                    <div class="three-col__item">
                        <article class="recent-posts__item">
                            <a href="<?php the_sub_field('page_link'); ?>" class="recent-posts__img">

                                <img src="<?php the_field('preview_image', $post_id); ?>" alt="<?php echo get_the_title($post_id); ?>"></a>
                            <a href="<?php the_sub_field('page_link'); ?>" class="recent-posts__title"><?php echo get_the_title($post_id); ?></a>
                            <div class="recent-posts__meta icon-text__wrap">
                                <date class="recent-posts__date"><?php echo get_the_date('', $post_id); ?></date>
                                <div class="recent-posts__views icon-text__wrap">
                                    <svg class="fill_grey" width="12px" height="8px"><use xlink:href="#icon-eye"></use></svg>
                                    <span><?php echo getPostViews($post_id); ?></span>
                                </div>
                            </div>
                        </article>
                    </div>
                   <?php  endwhile; ?>
                </div>
                <?php $blog_page_id = get_option( 'page_for_posts' ); ?>
                <div class="center-wrap bottom-btn-wrap"><a href="<?php the_permalink($blog_page_id); ?>" class="reg-btn"><?php the_field('recent_posts_block_-_button'); ?></a></div>
                
            </div>
        </div>
        <?php endif; ?>        

        
       <?php if( have_rows('inspire_block') ): ?>
            <div class="inspire__block">
                <div class="wrapper">
                    <div class="main-title-wrap">
                        <div class="main-title"><?php the_field('inspire_block_-_titlte'); ?></div>
                    </div>  
                    <!-- <div class="inspire__wrap flex-block flex-jc-sb flex-ai-c"> -->
                    <div class="inspire__wrap inspire__slider slick-preload js-inspire-slider">
                    <?php while ( have_rows('inspire_block') ) : the_row(); ?>
                        <div class="inspire__item"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
                       <?php  endwhile; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>  
       <?php if( get_field('seo_text_block') ): ?>
        <div class="seo-text__block">
            <div class="wrapper wrapper_860">
                <div class="seo-text__content content__block"><?php the_field('seo_text_block'); ?></div>
            </div>
        </div>
        <?php endif; ?>  


   
<?php
get_footer();
