<?php
/**
 * Template Name: Forwarding Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>

  <div class="info-feats__block bg_lt-blue">
    <div class="wrapper wrapper_860">
      <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_1_-_title'); ?></div>
          <div class="forwarding-types__wrap">
            <div class="main-title main-title_smallest"><?php the_field('block_1_-_subtitle'); ?></div>
            <?php if( have_rows('block_1_-_types') ): ?>
            <ul class="forwarding-types__list flex-block flex-jc-c">
              <?php while ( have_rows('block_1_-_types') ) : the_row(); ?>
              <li class="three-col__item center-wrap">
                <div class="forwarding-types__title"><?php the_sub_field('title'); ?></div>
              </li>
              <?php  endwhile; ?>
            </ul>
            <?php endif; ?> 
          </div>
      </div> 
    <?php if( have_rows('block_1') ): ?>
      <div class="info-feats__wrap flex-block">
        <?php while ( have_rows('block_1') ) : the_row(); ?>
          <div class="three-col__item">
            <div class="info-feats__item center-wrap">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="forwarding__item-title"><?php the_sub_field('title'); ?></div>
                <div class="forwarding__item-text"><?php the_sub_field('text'); ?></div>
            </div>
          </div>

        <?php  endwhile; ?>
         
        </div>
      </div>
    <?php endif; ?>  
    <div class="center-wrap bottom-btn-wrap"><a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php echo pll_e('requestform')?></a></div>
  </div>
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
