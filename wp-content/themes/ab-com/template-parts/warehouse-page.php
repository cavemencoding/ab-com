<?php
/**
 * Template Name: Warehouse Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block first-text__block_mb40 textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
<?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>

  <div class="info-feats__block info-feats__block_warehouse">
    <div class="wrapper wrapper_860">
    <?php if( have_rows('block_1') ): ?>
      <div class="info-feats__wrap flex-block flex-jc-c">
        <?php while ( have_rows('block_1') ) : the_row(); ?>
          <div class="three-col__item">
            <div class="info-feats__item center-wrap">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="warehouse__item-text"><?php the_sub_field('title'); ?></div>
            </div>
          </div>

        <?php  endwhile; ?>
         
        </div>



      </div>
    <?php endif; ?>  
  </div>
  <div class="info-feats__block info-feats__block_warehouse2 info-feats__block_pb50 bg_lt-blue">
    <div class="wrapper wrapper_860">
      <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_2_-_title'); ?></div>
      </div> 
      <div class="warehouse-simpletext center-wrap"><?php the_field('block_2_-_subitle'); ?></div>
    <?php if( have_rows('block_2') ): ?>
      <div class="info-feats__wrap flex-block">
        <?php while ( have_rows('block_2') ) : the_row(); ?>
          <div class="three-col__item">
            <div class="info-feats__item center-wrap">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="warehouse__item-text"><?php the_sub_field('title'); ?></div>
            </div>
          </div>

        <?php  endwhile; ?>
         
        </div>
      </div>
    <?php endif; ?>  
  </div>
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
