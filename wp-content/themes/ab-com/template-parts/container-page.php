<?php
/**
 * Template Name: Container Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>
<?php if( have_rows('block_1') ): ?>
  <div class="container__block">
    <div class="wrapper">
      <div class="main-title-wrap main-title-wrap_smaller main-title-wrap_npt">
        <div class="main-title"><?php the_field('block_1_-_title'); ?></div>
      </div>   
    <?php if( have_rows('block_1') ): ?>
    
      <div class="container__wrap flex-block flex-jc-c">
      <?php while ( have_rows('block_1') ) : the_row(); ?>

        <div class="two-col__item">
          <div class="container__item">
            <div class="container__item-header bg_lt-blue"><?php the_sub_field('title'); ?></div>
            <div class="container__item-img"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
            <div class="container__item-table">
              <table>
                <?php  $table_header = get_sub_field( 'table_header' );

                  if ( $table_header ) { ?>
                    <thead class="container-table__header">

                       <?php foreach ( $table_header['body'] as $tr ) { ?>
                          <tr>
                            <?php foreach ( $tr as $td ) { ?>
                              <th><?php echo $td['c']; ?></th>
                             <?php  }   ?>
                          </tr>
                      <?php } ?>
                    </thead>
                 <?php } ?>



              <?php if( have_rows('table_section') ): ?>
                <tbody>
                  <?php while ( have_rows('table_section') ) : the_row(); ?>
                    <tr>
                      <td colspan="4">
                        <table>
                          <thead class="container-table-sect__header">
                            <tr>
                              <th colspan="4"><?php the_sub_field('table_section_title'); ?></th>
                            </tr>
                          </thead>
                          <?php  $table_section_inner = get_sub_field( 'table_section_inner' );

                            if ( $table_section_inner ) { ?>
                              <tbody>

                                 <?php foreach ( $table_section_inner['body'] as $tr ) { ?>
                                    <tr>
                                      <?php foreach ( $tr as $td ) { ?>

                                        <td><?php echo $td['c']; ?></td>

                                 <?php } ?>
                                        </tr>
                                         <?php } ?>
                                    </tbody>
                                   <?php } ?>              
                                </table>
                              </td>
                          </tr>
                     <?php  endwhile; ?>
                    </tbody>
                <?php endif; ?>          
              </table>
            </div>
          </div>
        </div>
        <?php  endwhile; ?>
      </div>
      <?php endif; ?>    
    </div>
  </div>
<?php endif; ?>  
 <?php if( have_rows('block_2') ): ?>

<div class="info-feats__block bg_lt-blue">
    <div class="wrapper wrapper_860">
      <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_2_-_title'); ?></div>
      </div> 
      <div class="info-feats__wrap">
<?php while ( have_rows('block_2') ) : the_row();
$i++; ?>
<?php 
if ($i == 1 || $i == 3 || $i == 6) {
  echo '<div class="flex-block">';

}
if ( $i >= 3 && $i <= 5) {
  echo '<div class="three-col__item">';
} else {
  echo '<div class="two-col__item">';

}
?>
          
            <div class="info-feats__item center-wrap">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="info-feats__item-title"><?php the_sub_field('title'); ?></div>
            </div>
          </div>
          <?php 
if ($i == 2 || $i == 5) {
  echo '</div>';

}
?>
<?php  endwhile; ?>
         
      </div>
      <div class="center-wrap bottom-btn-wrap"><a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php echo pll_e('requestform')?></a></div>
      </div>
    </div>
    </div>
</div>
<?php endif; ?>  
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
