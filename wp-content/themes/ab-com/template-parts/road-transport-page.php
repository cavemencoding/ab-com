<?php
/**
 * Template Name: Road Transport Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>
<div class="geo-map__block bg_lt-blue">
    <div class="wrapper">
        <div class="main-title-wrap main-title-wrap_smaller">
            <div class="main-title">Geography of Services</div>
        </div> 
      <?php get_template_part( 'template-parts/geo-block', get_post_format() ); ?>
    </div>
</div>
<div class="wrapper">
  <?php if( get_field('enable_block_1') ): ?>
     <div class="geo-sizes__block">
        <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_1_-_title'); ?></div>
        </div> 
        <?php if( have_rows('block_1') ): ?>
      <div class="geo-sizes__wrap flex-block">
        <?php while ( have_rows('block_1') ) : the_row(); ?>
            <div class="four-col__item">
              <div class="geo-sizes__item center-wrap">
                <div class="geo-sizes__item-text"><?php the_sub_field('text'); ?></div>
                <div class="geo-sizes__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
              </div>
             </div>
        <?php  endwhile; ?>
       </div>
  <?php endif; ?>  
     </div>
  <?php endif; ?>

  <?php if( get_field('enable_block_2') ): ?>
      <div class="geo-deleveries__block">
        <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_2_-_title'); ?></div>
        </div> 
        <?php if( have_rows('block_2') ): ?>
      <div class="geo-deleveries__wrap flex-block">
      <?php while ( have_rows('block_2') ) : the_row(); ?>
        <div class="three-col__item">
          <div class="geo-deleveries__item bg_lt-blue center-wrap">
            <div class="geo-deleveries__item-icon">
             <img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('text'); ?>">
            </div>
            <div class="geo-deleveries__iten-text"><?php the_sub_field('text'); ?></div>
          </div>
        </div>      
      <?php  endwhile; ?>
      </div>
    <?php endif; ?>
    </div>
<?php endif; ?>    

</div>
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block seo-text__block_mt">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
