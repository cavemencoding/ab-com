<?php
/**
 * Template Name: Contact Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPr5HMalv0zV6XqvZOzmSFnW12IKVsC68"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/map.js"></script>

<!-- wp_enqueue_script( 'ab-com-map', get_template_directory_uri() . '/js/map.js', array(), '20151215', true ); -->

<?php if( have_rows('branches') ): ?>
<div class="branch__block">
  <div class="wrapper">
    <div class="branch__wrap flex-block flex-jc-c">
      <?php while ( have_rows('branches') ) : the_row(); ?>
      <div class="three-col__item">
        <div class="branch__item">
          <div class="branch__item-map">
            <?php $location = get_sub_field('map');
            if( !empty($location) ):
            ?>
                <div class="acf-map">
                  <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            <?php endif; ?>            
          </div>
          <div class="branch__item-hover center-wrap">
            <div class="branch__item-title"><?php the_sub_field('title'); ?></div>
            <div class="branch__item-desc"><?php the_sub_field('text'); ?></div>
          </div>
        </div>
      </div>
      <?php  endwhile; ?>
    </div>
  </div>
</div>
<?php endif; ?>  
<div class="contact-form__block bg_lt-blue">
  <div class="wrapper wrapper_770">
    <div class="main-title-wrap main-title-wrap_smaller">
      <div class="main-title main-title_smaller"><?php the_field('form_title'); ?></div>
    </div> 
    <form class="js-send-form" func-action="contact_form" novalidate>
    <div class="contact-form__wrap">
      <div class="flex-block">
        <div class="two-col__item">
          <div class="fake-placeholder__wrap">
            <input name="name" required type="text" class="form-input form-input_wht js-fake-placeholder">
            <label class="fake-placeholder__label"><?php the_field('form_label_1'); ?></label>
          </div>
        </div>
        <div class="two-col__item">
          <div class="fake-placeholder__wrap">
            <input name="email" required type="email" class="form-input form-input_wht js-fake-placeholder">
            <label class="fake-placeholder__label"><?php the_field('form_label_2'); ?></label>
          </div>
        </div>
      </div>
      <div class="flex-block">
        <div class="two-col__item">
          <div class="fake-placeholder__wrap">
            <input name="address" type="text" class="form-input form-input_wht js-fake-placeholder">
            <label class="fake-placeholder__label"><?php the_field('form_label_3'); ?></label>
          </div>
        </div>
        <div class="two-col__item">
          <div class="flex-block">
            <div class="small-col__item">
              <div class="fake-placeholder__wrap">
                <input name="zip" type="text" class="form-input form-input_wht js-fake-placeholder">
                <label class="fake-placeholder__label"><?php the_field('form_label_4'); ?></label>
              </div>
          </div>
            <div class="big-col__item">
              <div class="fake-placeholder__wrap">
                <input name="city" type="text" class="form-input form-input_wht js-fake-placeholder">
                <label class="fake-placeholder__label"><?php the_field('form_label_5'); ?></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <input name="subject" type="hidden" value="<?php the_field('form_subject'); ?>">
      <input name="emailTo" type="hidden" value="<?php the_field('form_email'); ?>">
      <div class="flex-block">
        <div class="one-col__item">
          <div class="fake-placeholder__wrap">
            <textarea name="remarks" required class="form-input form-input_wht js-fake-placeholder"></textarea>
            <label class="fake-placeholder__label"><?php the_field('form_label_6'); ?></label>
          </div>
        </div>
      </div>
      <div class="center-wrap form-btn-wrap"><button class="reg-btn"><?php the_field('form_button'); ?></button></div>
      <div class="center-wrap notify__block">
        <span class="notify__message notify__message_1 notify__message_err"><?php the_field('form_required_notify'); ?></span>
        <span class="notify__message notify__message_2 notify__message_err"><?php the_field('form_email_notify'); ?></span>
        <span class="notify__message notify__message_4 notify__message_err">There was an error trying to send your message. Please try again later.</span>
      </div>
    </div>
    </form>
  </div>
</div>
<?php
get_footer();
