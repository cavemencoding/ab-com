 <?php $main_page_id = get_option( 'page_on_front' ); ?>

<?php if( have_rows('map_block', $main_page_id) ): ?>
    <div class="front-geo__wrap">
        <div class="tab-header flex-block flex-jc-c">
<?php while ( have_rows('map_block', $main_page_id) ) : the_row(); $i++; ?>
        <button class="tab__item front-geo_tab-btn  <?php if($i == 1) {echo 'front-geo_tab-btn-dk tab__item_active'; } ?> js-tab__item" tab-id="<?php echo $i; ?>"><?php the_sub_field('tab_header'); ?></button>

<?php  endwhile; ?>
        </div>  
<?php endif; ?>   
<?php if( have_rows('map_block', $main_page_id) ): ?>
        <div class="tab-body">
            <?php while ( have_rows('map_block', $main_page_id) ) : the_row(); $j++; ?>
            <div class="tab__pane front-geo__tab-pane js-tab__pane <?php if($j == 1) {echo 'tab__pane_active'; } ?>" tab-target-id="<?php echo $j; ?>"> 
                <div class="front-geo__header"><img <?php if(!wp_is_mobile()){ echo 'style="width: 1051px; height: 553px"';}?> src="<?php echo get_template_directory_uri(); ?>/images/map<?php echo $j; ?>.<?php if(wp_is_mobile()){echo "png";}else{echo "svg";} ?>" alt=""></div>
                <div class="front-geo__body  flex-block flex-jc-c">
                    <div class="front-geo__list">
                        <div class="front-geo__list-title"><?php the_sub_field('list_title1'); ?></div>
                        <div class="front-geo__list-item">
                            <?php the_sub_field('list1'); ?>
                        </div>
                    </div>
                    <div class="front-geo__list">
                        <div class="front-geo__list-title"><?php the_sub_field('list_title2'); ?></div>
                        <div class="front-geo__list-item">
                            <?php the_sub_field('list2'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php  endwhile; ?>
         </div>  
    </div>
<?php endif; ?>  
