<?php
/**
 * Template Name: Racking Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ab-com
 */

get_header(); ?>


<div class="first-text__block textmarkup__block">
    <div class="wrapper wrapper_860">
        <div class="content__block">
        <?php
      while ( have_posts() ) : the_post();

        the_content();

      endwhile; // End of the loop.
      ?></div>
    </div>
    <?php get_template_part( 'template-parts/manager-block', get_post_format() ); ?>
</div>

  <div class="info-feats__block info-feats__block_pb50">
    <div class="wrapper">
      <div class="main-title-wrap main-title-wrap_smaller main-title-wrap_pt0">
          <div class="main-title"><?php the_field('block_1_-_title'); ?></div>
      </div> 
    <?php if( have_rows('block_1') ): ?>
      <div class="racking-product__wrap info-feats__wrap flex-block flex-jc-c">
        <?php while ( have_rows('block_1') ) : the_row(); ?>
          <div class="three-col__item">
            <div class="product__item">
              <div class="product__item-img"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
              <div class="product__item-content">
                <div class="product__item-header bg_lt-blue"><?php the_sub_field('title'); ?></div>
                  <?php if( have_rows('product_row') ): ?>
                <div class="product__item-body">
                  <?php while ( have_rows('product_row') ) : the_row(); ?>
                  <div class="product__item-row product-info__row">
                    <div class="product-info__row-title"><?php the_sub_field('title'); ?></div>
                    <div class="product-info__row-text"><?php the_sub_field('text'); ?></div>
                  </div>
                  <?php  endwhile; ?>
                </div>
                  <?php endif; ?>  
                </div>
              </div>
          </div>

        <?php  endwhile; ?>
         
        </div>
      </div>
    <?php endif; ?>  
  </div>
  <div class="info-feats__block info-feats__block_pb50 bg_lt-blue">
    <div class="wrapper">
      <div class="main-title-wrap main-title-wrap_smaller">
          <div class="main-title"><?php the_field('block_2_-_title'); ?></div>
      </div> 
    <?php if( have_rows('block_2') ): ?>
      <div class="info-feats__wrap flex-block">
        <?php while ( have_rows('block_2') ) : the_row(); ?>
          <div class="three-col__item">
            <div class="info-feats__item center-wrap">
                <div class="info-feats__item-icon">
                  <img src="<?php the_sub_field('icon'); ?>" alt="">
                </div>
                <div class="forwarding__item-text"><?php the_sub_field('text'); ?></div>
            </div>
          </div>

        <?php  endwhile; ?>
         
        </div>
      </div>
    <?php endif; ?>  
    <div class="center-wrap bottom-btn-wrap"><a href="#"  modal-target="application-form-modal1" class="reg-btn js-modal-link"><?php echo pll_e('requestform')?></a></div>
  </div>
  <div class="warranty__block">
    <div class="wrapper wrapper_860">
      <div class="warranty__header flex-block flex-jc-c flex-ai-c">
        <div class="warranty-header__num"><?php the_field('warranty_number'); ?></div>
        <div class="warranty-header__text">
          <div class="warranty-header__text1"><?php the_field('warranty_number_text1'); ?></div>
          <div class="warranty-header__text2"><?php the_field('warranty_number_text2'); ?></div>
        </div>
      </div>
      <div class="warranty__body">
        <div class="warranty-body__title center-wrap"><?php the_field('warranty_list_title'); ?></div>
        <?php if( have_rows('warranty_list') ): ?>
        <ul class="warranty-body__list flex-block">
          <?php while ( have_rows('warranty_list') ) : the_row(); ?>        
            <li class="two-col__item">
              <div class="warranty-body__list-item">
                <svg class="fill_blue" width="11px" height="16px"><use xlink:href="#icon-check"></use></svg>
                <span><?php the_sub_field('text'); ?></span>
              </div>
              </li>
          <?php  endwhile; ?>
        </ul> 
        <?php endif; ?>          
      </div>
      <div class="warranty__footer">
        <div class="flex-block">
          <?php if( have_rows('warranty_marks') ): ?>
            <?php while ( have_rows('warranty_marks') ) : the_row(); ?>
            <div class="three-col__item">
              <div class="warranty-mark__item flex-block flex-jc-c flex-ai-c">
                <div class="warranty-mark__item-img"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
              </div>
            </div>
            <?php  endwhile; ?>
          <?php endif; ?>       
            <div class="one-col__item">
              <div class="warranty-mark__item flex-block flex-jc-c flex-ai-c">
                <div class="warranty-mark__item-text center-wrap"><strong><?php the_field('warranty_marks_text1'); ?></strong><span><?php the_field('warranty_marks_text2'); ?></span></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
<?php if( get_field('seo_text') ): ?>
  <div class="seo-text__block">
      <div class="wrapper wrapper_860">
          <div class="seo-text__content content__block"><?php the_field('seo_text'); ?></div>
      </div>
  </div>
<?php endif; ?>  

<?php
get_footer();
