<article class="recent-posts__item">
    <a href="<?php the_permalink(); ?>" class="recent-posts__img"><img src="<?php the_field('preview_image'); ?>" alt="<?php the_title(); ?>"></a>
    <a href="<?php the_permalink(); ?>" class="recent-posts__title"><?php the_title(); ?></a>
    <div class="recent-posts__meta icon-text__wrap">
        <div class="recent-posts__date"><?php echo get_the_date(''); ?></div>
        <div class="recent-posts__views icon-text__wrap">
            <svg class="fill_grey" width="12px" height="8px"><use xlink:href="#icon-eye"></use></svg>
            <span><?php echo getPostViews(GET_THE_ID()); ?></span>
        </div>
    </div>
</article>
    