<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AB-Com
 */

?>

    </main>
    <!--Footer -->
    <?php  if (!is_404() ) { ?>
    <footer class="main-footer__block">
        <div class="wrapper">
            <div class="footer__top flex-block">
                <div class="two-col__item">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  class="main-logo main-logo_footer"><img style="width: 173px; height: 58px;" src="<?php echo get_template_directory_uri(); ?>/images/main-logo.svg" alt="<?php bloginfo( 'name' ); ?>"></a>

                    <?php $main_page_id = get_option( 'page_on_front' ); ?>

                    <?php if( get_field('address', $main_page_id) ): ?>
                        <div class="footer-subtitle footer-subtitle_m">
                            <?php the_field('address', $main_page_id); ?>
                        </div>
                    <?php endif; ?>
                    <div class="social__block icon-text__wrap">

                        <?php if( get_field('facebook_link', $main_page_id) ): ?>
                         <a target="_blank" href="<?php the_field('facebook_link', $main_page_id); ?>">
                            <svg version="1.1" width="25px" height="25px" id="soc-icon1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                            <style type="text/css">
                                #soc-icon1 .st0{fill:#FFFFFF;}
                                #soc-icon1 .st1{fill:#3A589B;}
                            </style>
                            <rect x="15.7" y="10.8" class="st0" width="29" height="38.7"/>
                            <path class="st1" d="M25.8,46.3h6.7V30H37l0.6-5.6h-5.1l0-2.8c0-1.5,0.1-2.3,2.2-2.3h2.8v-5.6h-4.5c-5.4,0-7.3,2.7-7.3,7.3v3.4h-3.4
                                V30h3.4V46.3z M30,60C13.4,60,0,46.6,0,30C0,13.4,13.4,0,30,0s30,13.4,30,30C60,46.6,46.6,60,30,60z"/>
                            </svg>
                        </a>    
                        <?php endif; ?>
                        <?php if( get_field('instagram_link', $main_page_id) ): ?>
                        <a target="_blank" href="<?php the_field('instagram_link', $main_page_id); ?>">
                        <svg version="1.1" width="25px" height="25px" id="soc-icon2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 473.4 473.4" style="enable-background:new 0 0 473.4 473.4;" xml:space="preserve">
                            <style type="text/css">
                                #soc-icon2 .st0{fill:url(#SVGID_1_);}
                                #soc-icon2 .st1{fill:#FFFFFF;}
                            </style>
                            <g id="Edges">
                            </g>
                            <g id="Background_1_">
                                
                                    <radialGradient id="SVGID_1_" cx="1074.8053" cy="1260.2512" r="684.4449" gradientTransform="matrix(5.233596e-02 -0.9986 -0.8488 -4.448556e-02 1022.0965 1559.345)" gradientUnits="userSpaceOnUse">
                                    <stop  offset="0" style="stop-color:#FED576"/>
                                    <stop  offset="0.2634" style="stop-color:#F47133"/>
                                    <stop  offset="0.6091" style="stop-color:#BC3081"/>
                                    <stop  offset="1" style="stop-color:#4C63D2"/>
                                </radialGradient>
                                <path class="st0" d="M0,230.7v12.8c3.3,122.4,101.9,220.7,219.2,229.9h36.1c118-9.3,214.9-108.5,218.1-230.1v-12.6
                                    C470.2,106.9,369.9,6.5,249.1,0.3C119.1-6.3,3.6,98,0,230.7z"/>
                            </g>
                            <g id="Symbol">
                                <g>
                                    <path class="st1" d="M237.8,118.9c37.7,0,42.2,0.1,57.1,0.8c13.8,0.6,21.2,3,26.2,4.9c6.6,2.6,11.3,5.7,16.2,10.7
                                        c4.9,5,8,9.8,10.6,16.5c1.9,5.1,4.2,12.7,4.9,26.7c0.7,15.2,0.8,19.7,0.8,58.1s-0.1,42.9-0.8,58.1c-0.6,14-2.9,21.6-4.9,26.7
                                        c-2.6,6.7-5.6,11.5-10.6,16.5c-4.9,5-9.6,8.1-16.2,10.7c-5,2-12.5,4.3-26.2,4.9c-14.9,0.7-19.4,0.8-57.1,0.8s-42.2-0.1-57.1-0.8
                                        c-13.8-0.6-21.2-3-26.2-4.9c-6.6-2.6-11.3-5.7-16.2-10.7s-8-9.8-10.6-16.5c-1.9-5.1-4.2-12.7-4.9-26.7
                                        c-0.7-15.2-0.8-19.7-0.8-58.1s0.1-42.9,0.8-58.1c0.6-14,2.9-21.6,4.9-26.7c2.6-6.7,5.6-11.5,10.6-16.5c4.9-5,9.6-8.1,16.2-10.7
                                        c5-2,12.5-4.3,26.2-4.9C195.6,119.1,200.1,118.9,237.8,118.9 M237.8,93c-38.4,0-43.2,0.2-58.2,0.9c-15,0.7-25.3,3.1-34.3,6.7
                                        c-9.3,3.7-17.2,8.6-25,16.6c-7.9,8-12.7,16-16.3,25.5c-3.5,9.1-5.9,19.6-6.6,34.9s-0.8,20.2-0.8,59.2s0.2,43.9,0.8,59.2
                                        c0.7,15.3,3.1,25.7,6.6,34.9c3.6,9.4,8.4,17.5,16.3,25.5c7.9,8,15.7,12.9,25,16.6c9,3.6,19.3,6,34.3,6.7
                                        c15.1,0.7,19.9,0.9,58.2,0.9s43.2-0.2,58.2-0.9c15-0.7,25.3-3.1,34.3-6.7c9.3-3.7,17.2-8.6,25-16.6c7.9-8,12.7-16,16.3-25.5
                                        c3.5-9.1,5.9-19.6,6.6-34.9s0.8-20.2,0.8-59.2s-0.2-43.9-0.8-59.2c-0.7-15.3-3.1-25.7-6.6-34.9c-3.6-9.4-8.4-17.5-16.3-25.5
                                        s-15.7-12.9-25-16.6c-9-3.6-19.3-6-34.3-6.7C281,93.2,276.1,93,237.8,93L237.8,93z"/>
                                    <path class="st1" d="M237.8,162.9c-40.1,0-72.5,33-72.5,73.8s32.5,73.8,72.5,73.8s72.5-33,72.5-73.8S277.8,162.9,237.8,162.9z
                                         M237.8,284.6c-26,0-47.1-21.4-47.1-47.9s21.1-47.9,47.1-47.9s47.1,21.4,47.1,47.9S263.8,284.6,237.8,284.6z"/>
                                    <ellipse class="st1" cx="313.2" cy="160" rx="16.9" ry="17.2"/>
                                </g>
                            </g>
                            </svg>
                        </a>
                        <?php endif; ?> 

                        <?php if( get_field('linkedin_link', $main_page_id) ): ?>
                            <a target="_blank" href="<?php the_field('linkedin_link', $main_page_id); ?>">
                                <svg version="1.1" width="25px" height="25px" id="soc-icon3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                                <style type="text/css">
                                    #soc-icon3 .st0{fill:#FFFFFF;}
                                    #soc-icon3 .st1{fill:#007AB9;}
                                </style>
                                <rect x="12.8" y="12.5" class="st0" width="37.5" height="34.5"/>
                                <path class="st1" d="M46.8,44.1V32.4c0-6.3-3.3-9.2-7.8-9.2c-3.6,0-5.2,2-6.1,3.4v-2.9h-6.8c0.1,1.9,0,20.4,0,20.4h6.8V32.7
                                    c0-0.6,0-1.2,0.2-1.7c0.5-1.2,1.6-2.5,3.5-2.5c2.5,0,3.4,1.9,3.4,4.6v10.9L46.8,44.1L46.8,44.1z M19,20.9c2.4,0,3.8-1.6,3.8-3.5
                                    c0-2-1.5-3.5-3.8-3.5s-3.8,1.5-3.8,3.5C15.2,19.4,16.6,20.9,19,20.9L19,20.9z M30,60C13.4,60,0,46.6,0,30C0,13.4,13.4,0,30,0
                                    s30,13.4,30,30C60,46.6,46.6,60,30,60z M22.4,44.1V23.7h-6.8v20.4H22.4z"/>
                                </svg>

                            </a>    
                            
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="two-col__item flex-block flex-jc-sb">
                    <div class="footer-col">

                        <?php if( get_field('header_1', $main_page_id) ): ?>
                            <div class="footer-title"><?php the_field('header_1', $main_page_id); ?></div>
                        <?php endif; ?>  
                          


                        <?php if( get_field('subheader_1', $main_page_id) ): ?>
                            <div class="footer-subtitle"><?php the_field('subheader_1', $main_page_id); ?></div>
                        <?php endif; ?>  
                          


                        <?php if( get_field('bold_text_1', $main_page_id) ): ?>
                            <div class="footer-boldtext"><?php the_field('bold_text_1', $main_page_id); ?></div>
                        <?php endif; ?>  
                        <?php if( have_rows('list_1', $main_page_id) ): ?>
                            <ul class="footer-list">
                        <?php while ( have_rows('list_1', $main_page_id) ) : the_row(); ?>
                            <li>
                                <?php $icon = get_sub_field('icon');
                                if ($icon == 'phone') {
                                    $icon_svg = '<svg class="fill_yellow" width="12px" height="12px"><use xlink:href="#icon-phone"></use></svg>';
                                } elseif($icon == 'smartphone') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="14px"><use xlink:href="#icon-phone2"></use></svg>';
                                } elseif($icon == 'fax') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="13px"><use xlink:href="#icon-fax"></use></svg>';
                                } elseif($icon == 'letter') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="10px"><use xlink:href="#icon-letter"></use></svg>';
                                }

                                $content = get_sub_field('content');
                                $content = str_replace(' ', '', $content);
                                 ?>
                                
                                <a href="<?php if($icon == 'letter'){echo 'mailto';}else{echo 'tel';}?>:<?php echo $content; ?>">
                                    
                                    <?php echo $icon_svg; ?>
                                    <span><?php the_sub_field('content'); ?></span>
                                </a>
                            </li>
                        <?php  endwhile; ?>
                        </ul>
                        <?php endif; ?>                          
                    </div>
                    <div class="footer-col">

                        <?php if( get_field('header_2', $main_page_id) ): ?>
                            <div class="footer-title"><?php the_field('header_2', $main_page_id); ?></div>
                        <?php endif; ?>  
                          


                        <?php if( get_field('subheader_2', $main_page_id) ): ?>
                            <div class="footer-subtitle"><?php the_field('subheader_2', $main_page_id); ?></div>
                        <?php endif; ?>  
                          


                        <?php if( get_field('bold_text_2', $main_page_id) ): ?>
                            <div class="footer-boldtext"><?php the_field('bold_text_2', $main_page_id); ?></div>
                        <?php endif; ?>  
                        <?php if( have_rows('list_2', $main_page_id) ): ?>
                            <ul class="footer-list">
                        <?php while ( have_rows('list_2', $main_page_id) ) : the_row(); ?>
                            <li>
                                <?php $icon = get_sub_field('icon');
                                if ($icon == 'phone') {
                                    $icon_svg = '<svg class="fill_yellow" width="12px" height="12px"><use xlink:href="#icon-phone"></use></svg>';
                                } elseif($icon == 'smartphone') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="14px"><use xlink:href="#icon-phone2"></use></svg>';
                                } elseif($icon == 'fax') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="13px"><use xlink:href="#icon-fax"></use></svg>';
                                } elseif($icon == 'letter') {
                                    $icon_svg = '<svg class="fill_yellow" width="13px" height="10px"><use xlink:href="#icon-letter"></use></svg>';
                                }

                                $content = get_sub_field('content');
                                $content = str_replace(' ', '', $content);
                                 ?>
                                
                                <a href="<?php if($icon == 'letter'){echo 'mailto';}else{echo 'tel';}?>:<?php echo $content; ?>">
                                    
                                    <?php echo $icon_svg; ?>
                                    <span><?php the_sub_field('content'); ?></span>
                                </a>
                            </li>
                        <?php  endwhile; ?>
                        </ul>
                        <?php endif; ?>                          
                    </div>
                </div>
            </div>
            <div class="footer__bottom flex-block flex-jc-sb flex-ai-c">
                <div class="copyright">&copy; AB ComServices 2017</div>
                <nav class="footer-nav__block">
                	<?php  wp_nav_menu( array( 'menu' => 'menu_footer_'.pll_current_language(), 'menu_class' => 'nav__list nav__list_footer', 'container' => false ) ); ?>
                </nav>
                <div class="develop"><span><?php the_field('develop_text', $main_page_id); ?> </span><a target="_blank" href="http://cavemencoding.com/">C a v e m e n</a></div>
            </div>
        </div>
    </footer>
    <?php } ?>
<!--     <div class="manager__block manager__block_fixed flex-block">
        <div class="manager__icon">
            <div class="manager__img"><img src="<?php echo get_template_directory_uri(); ?>/images/manager-img.jpg" alt=""></div>
            <div class="manager__title">Manager</div>
        </div>
        <div class="manager__body">
            <div class="manager__text">Find out the cost of your project</div>
            <div class="manager__btn"><a href="" class="reg-btn">REQUEST OFFER</a></div>
        </div>
    </div> -->
<div style="display:none" class="hidden">
  <div class="application-form-modal1">
      <div class="box-modal box-modal_wide">
        <button class="box-modal_close arcticmodal-close"></button>
        <div class="modal-header center-wrap">
            <div class="main-title main-title_smaller"><?php the_field('form_title', $main_page_id); ?></div>
            <div class="modal-header__text"><?php the_field('form_subtitle', $main_page_id); ?></div>
        </div>
        <div class="modal-body">
            <form class="js-send-form" func-action="application_form" novalidate>
              <input name="subject" type="hidden" value="<?php the_field('form_subject', $main_page_id); ?>">
              <input name="emailTo" type="hidden" value="<?php the_field('form_email', $main_page_id); ?>">
                <div class="contact-form__wrap">
                  <div class="flex-block">
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="name" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_1', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="country" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_2', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="email" required type="email" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_3', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="phone" required type="tel" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_4', $main_page_id); ?></label>
                      </div>
                    </div>

                        <?php
                        if (pll_current_language() == 'en') {
                            $customservices_page_id = 146;
                        } else if(pll_current_language() == 'ru'){
                            $customservices_page_id = 448;
                        } else if(pll_current_language() == 'zn'){
                            $customservices_page_id = 492;
                        } else if(pll_current_language() == 'nd'){
                            $customservices_page_id = 490;
                        }
                        if (is_page($customservices_page_id)) { ?>
                            
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="procedure" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_5'); ?></label>
                          </div>
                        </div> 
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="commodityCode" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_6'); ?></label>
                          </div>
                        </div> 
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="commodity" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_7'); ?></label>
                          </div>
                        </div> 
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="origin" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_8'); ?></label>
                          </div>
                        </div>
                    </div>
                        <?php } else { ?>

                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="departure" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_5', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="destination" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_6', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="сommodity" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_7', $main_page_id); ?></label>
                      </div>
                    </div>
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="packing" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_8', $main_page_id); ?></label>
                      </div>
                    </div>
                  </div>
                  <div class="flex-block">
                    <div class="two-col__item">
                      <div class="flex-block">
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="quantity" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_9', $main_page_id); ?></label>
                          </div>
                        </div>
                        <div class="two-col__item">
                          <div class="fake-placeholder__wrap">
                            <input name="weight" type="text" class="form-input js-fake-placeholder">
                            <label class="fake-placeholder__label"><?php the_field('form_label_10', $main_page_id); ?></label>
                          </div>
                        </div>
                      </div>
                  </div>                  
                    <div class="two-col__item">
                      <div class="fake-placeholder__wrap">
                        <input name="procedure" type="text" class="form-input js-fake-placeholder">
                        <label class="fake-placeholder__label"><?php the_field('form_label_11', $main_page_id); ?></label>
                      </div>
                    </div>
                </div>
                  <div class="flex-block">
                    <div class="one-col__item">
                      <div class="fake-placeholder__wrap">
                        <textarea name="info" class="form-input js-fake-placeholder"></textarea>
                        <label class="fake-placeholder__label"><?php the_field('form_label_12', $main_page_id); ?></label>
                      </div>
                    </div>
                  </div>

                <?php } ?>

                  <div class="center-wrap form-btn-wrap"><button class="reg-btn"><?php the_field('form_button', $main_page_id); ?></button></div>
                  <div class="center-wrap notify__block">
                    <span class="notify__message notify__message_1 notify__message_err"><?php the_field('form_required_notify', $main_page_id); ?></span>
                    <span class="notify__message notify__message_2 notify__message_err"><?php the_field('form_email_notify', $main_page_id); ?></span>
                    <span class="notify__message notify__message_4 notify__message_err">There was an error trying to send your message. Please try again later.</span>
                  </div>
                </div>
            </form>
        </div>
      </div>
  </div>
    <div class="thnx-modal">
        <div class="box-modal">
            <button class="box-modal_close arcticmodal-close"></button>
            <div class="thnx-modal__block center-wrap">
                <div class="thnx-modal__icon"><svg class="fill_blue" width="49px" height="49px"><use xlink:href="#icon-check3"></use></svg></div>
                <div class="thnx-modal__title main-title main-title_smaller"><?php the_field('modal_thnx_header', $main_page_id); ?></div>
                <div class="thnx-modal__text"><?php the_field('modal_thnx_text', $main_page_id); ?></div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

<script> 
    $(function(){
      $(".js-svg-sprite").load("<?php echo get_template_directory_uri(); ?>/svg/symbol_sprite.html"); 
    });
</script> 
<div class="js-svg-sprite"></div>
</body>
</html>
