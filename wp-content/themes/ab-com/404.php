<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package devils
 */

get_header(); ?>

	<div class="wrapper wrapper_not-found flex-block flex-jc-c flex-ai-c">
      <div class="not-found-block center-wrap">
            <div class="not-found-nums">404</div>
            <div class="not-found-title">Page not found</div>
            <div class="not-found-text">Unfortunately, this page does not exist.<br> Please check your URL or return to Home Page</div>
			<div class="center-wrap">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="reg-btn reg-btn_empty">Return to homepage</a>
			</div>
      </div>
	</div>

<?php
get_footer();
