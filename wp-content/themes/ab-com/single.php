<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AB-Com
 */
get_header(); ?>

<article class="blog-single__block">
	<div class="wrapper wrapper_700">
		<div class="content__block">
			
	   	<?php
			while ( have_posts() ) : the_post();

				the_content();

			endwhile; // End of the loop.
			?>
			 <?php  wp_reset_query(); ?>
		</div>
		<div class="blog-single__footer flex-block flex-jc-sb">
			<div class="post-ratings__block">
				<div class="post-ratings-text-title">Rate this article:</div>
				<div class="post-ratings-block"><?php if(function_exists('the_ratings')) { the_ratings(); } ?></div>
			</div>
			<div class="social-share__block">
				<a href="javascript: void(0);" data-layout="button" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');" class="social-share__btn social-share__btn_tw">
                    <svg class="fill_wht" width="14px" height="11px"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use></svg>
                    <span>Twitter</span>
                </a>
				<a href="javascript: void(0);" data-layout="button" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');" class="social-share__btn social-share__btn_fb">
                    <svg class="fill_wht" width="8px" height="15px"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use></svg>
                    <span>Share</span>
                </a>
			</div>
		</div>
	</div>
</article>
 <?php if( have_rows('related_posts') ): ?>
<div class="blog-single__related bg_lt-blue">
    <div class="wrapper">
        <div class="main-title-wrap main-title-wrap_smaller">
            <div class="main-title main-title_smaller">Related posts</div>
        </div>  
        <div class="recent-posts__wrap flex-block">
        <?php while ( have_rows('related_posts') ) : the_row(); ?>
            <?php $post_id = get_sub_field('page_link', false, false); ?>

            <div class="three-col__item">
                <article class="recent-posts__item">
                    <a href="<?php the_sub_field('page_link'); ?>" class="recent-posts__img">

                        <img src="<?php the_field('preview_image', $post_id); ?>" alt="<?php echo get_the_title($post_id); ?>"></a>
                    <a href="#" class="recent-posts__title"><?php echo get_the_title($post_id); ?></a>
                    <div class="recent-posts__meta icon-text__wrap">
                        <date class="recent-posts__date"><?php echo get_the_date('', $post_id); ?></date>
                        <div class="recent-posts__views icon-text__wrap">
                            <svg class="fill_grey" width="12px" height="8px"><use xlink:href="#icon-eye"></use></svg>
                            <span><?php echo getPostViews($post_id); ?></span>
                        </div>
                    </div>
                </article>
            </div>
           <?php  endwhile; ?>
        </div>
    </div>
</div>
 <?php endif; ?>   
<?php setPostViews(get_the_ID()); ?>
<?php
get_footer();
