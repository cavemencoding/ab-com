// Modules
var gulp = require('gulp'),
    connect = require('gulp-connect'),
    sass = require('gulp-sass');
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber');
    minifycss    = require('gulp-minify-css'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglifyjs'),
    svgSprite = require('gulp-svg-sprites'),
    svgmin = require('gulp-svgmin'),
    rsp = require('remove-svg-properties').stream,
    replace = require('gulp-replace');

// Local server
gulp.task('connect', function () {
  connect.server({
    root: ['src'],
    livereload: true
  });
});

// HTML
// gulp.task('html', function () {
  // gulp.src('*.html')
    // .pipe(gulp.dest('../src/'))
    // .pipe(connect.reload());
// });

// SVG
gulp.task('svgSpriteBuild', function () {
  return gulp.src('svg/*.svg')
    // minify svg
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(rsp.remove({
        properties: [rsp.PROPS_FILL, rsp.PROPS_STROKE]
    }))
    .pipe(svgSprite({
        mode: "symbols",
        preview: false,
        selector: "icon-%f",
        svg: {
          symbols: 'symbol_sprite.html'
        }
      }
    ))
    .pipe(gulp.dest('../svg'));
});

// SASS
gulp.task('sass', function () {
     gulp.src('sass/style.sass')
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({suffix: '.min', prefix : ''}))
    .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
    .pipe(minifycss())
    .pipe(gulp.dest('../'))
    .pipe(connect.reload());
});

// Javascripts
gulp.task('javascripts', function () {
  gulp.src('js/**/*.js')
    .pipe(uglify().on('error', function(uglify) {
        console.error(uglify.message);
        this.emit('end');
    }))
    .pipe(rename({suffix: '.min', prefix : ''}))
    .pipe(gulp.dest('../js'))
    .pipe(connect.reload());
});

// Watching
// gulp.watch(['*.html'], ['html']);
gulp.watch(['sass/*.sass'], ['sass']);
gulp.watch(['svg/*.svg'], ['svgSpriteBuild']);
gulp.watch(['js/*.js'], ['javascripts']);

// Run
gulp.task('default', ['connect', 'sass', 'svgSpriteBuild', 'javascripts']);