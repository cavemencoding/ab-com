$(document).ready(function(){

  // send form
  $('.js-send-form').on('submit', function(){
    var form = $(this),
        formData = {},
        email = $(this).find('input[name="email"]');

      form.find('.notify__message').hide();
      form.find('[required]').each(function(){
              if ($(this).val() == '') {
                $(this).addClass('err');
              } else {
                $(this).removeClass('err');
              }
            });
      if (form.find('[name]').hasClass('err')) {
        fadeInfadeOut(form, '.notify__message_1')
      } else if(!emailValidate(email.val())) {
        email.addClass('err');
        fadeInfadeOut(form, '.notify__message_2')

      } else {

        form.find('[name]').each(function(){
          var name = $(this).attr('name');
          formData[name] = $(this).val();
        });
        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: "send_form",
                data: formData
            },
            success: function(e) {
                if("success" == e ) { 
                  form.find('[name]:not([type="hidden"])').val('');
                  form.find('[name]').removeClass('populated');
                  $.arcticmodal('close');
                  $('.thnx-modal').arcticmodal({
                    beforeOpen: function(data, el) {

                      $('body').addClass('modal-open');
                    },
                    afterClose: function(data, el) {
                      $('body').removeClass('modal-open');
                    }
                  });
                } else if ("error" == e)  {
                  fadeInfadeOut(form, '.notify__message_4')
                }
            }
        })
      }
    return false;
  });



  // fake placeholder
  $('.js-fake-placeholder').on('change',function(){
    if ($(this).val() != '') {
      $(this).addClass('populated');
    } else {
      
      $(this).removeClass('populated');
    }
  });


  // niceSelect
  $('select').niceSelect();

  // language block toggle icons
  (function(){
    var curLang = $('#lang_choice_1').find(':selected').text();
    $('.language__block .current').addClass(curLang);
    $('.language__block').on('click','.option',function(){
      var lan = $(this).attr('data-value');
      console.log(lan);
      $(this).parent().prev().removeClass('en ru zh nl').addClass(lan);
    });
  }())

  // tabs
  $('.js-tab__item').on('click',function(){
    $('.js-tab__item').removeClass('tab__item_active');
    $(this).addClass('tab__item_active');
    var id =  $(this).attr('tab-id');
    $('.js-tab__pane').hide();
    $('.js-tab__pane[tab-target-id="'+id+'"]').fadeIn(0);
    return false;
  });

  // nav menu in mobile
  $('.hamburger').on('click', function(){
    if ($(this).hasClass('is-active')) {
      $(".main-header__block").css('position','absolute');
      $("html, body").css({'overflow':'visible','height':'auto'});
      $(this).removeClass('is-active');
      $('.nav__list_header').removeClass('active');
    } else {
      $(".main-header__block").css('position','static');
      $("html, body").css({'overflow':'hidden','height':'100%'});
      $("html").css({'overflow-y':'scroll'});
      $(this).addClass('is-active');
      $('.nav__list_header').addClass('active')
    }
  });





  // front page slider
   $('.js-front-slider').slick({
    fade: true,
    speed: 3000
  });
  
  // front page partners carousel
   $('.js-inspire-slider').slick({
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 5,
  responsive: [
    {
      breakpoint: 997,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }

  ]
});
  
  // dropdown windwow
  // $('.js-dropdown-btn').on('click', function(){
      // $(this).toggleClass('open').next().toggleClass('open');   
// 
  // });
  // $(document).click(function(event) {
    // if (!$(event.target).closest(".dropdown-block").length) {
      // $(".dropdown-wrap").removeClass('open');
      // $(".dropdown-btn").removeClass('open');
    // }
// 
  // });  
  // phone mask
	// $('.js-phone-mask').mask("+38099-999-99-99");

  // modal windows
  $('.js-modal-link').on('click',function(){
    var t = $(this).attr('modal-target');
    $.arcticmodal('close');
    $('.' + t).arcticmodal({
      beforeOpen: function(data, el) {

        $('body').addClass('modal-open');
      },
      afterClose: function(data, el) {
        $('body').removeClass('modal-open');
      }
    });
    return false;
  });  
});




function emailValidate(email) {
    var patt = /^.+@.+[.].{2,}$/i;
    return patt.test(email);
}


function fadeInfadeOut(form, el) {
    form.find(el).stop().fadeIn().delay(2500).fadeOut();
}

