<?php
/**
 * AB-Com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AB-Com
 */

if ( ! function_exists( 'ab_com_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ab_com_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on AB-Com, use a find and replace
		 * to change 'ab-com' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ab-com', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'ab-com' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ab_com_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'ab_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ab_com_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ab_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'ab_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ab_com_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ab-com' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ab-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ab_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ab_com_scripts() {
	wp_enqueue_style( 'ab-com-style', get_template_directory_uri() . '/style.min.css');

	wp_enqueue_script( 'ab-com-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ab-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	wp_enqueue_script( 'ab-com-jquery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'ab-com-arcticmodal', get_template_directory_uri() . '/js/jquery.arcticmodal-0.3.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'ab-com-slick', get_template_directory_uri() . '/js/slick.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'ab-com-nice-select', get_template_directory_uri() . '/js/jquery.nice-select.min.js', array(), '20151215', true );

	wp_enqueue_script( 'ab-com-scripts', get_template_directory_uri() . '/js/scripts.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ab_com_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}




// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


// ajax load more
function true_load_posts(){
  $count = $_POST['count'];
  $showNext = $_POST['showNext'];
  $args = unserialize(stripslashes($_POST['query']));
  $args['posts_per_page'] = $showNext;
  $args['offset'] = $count;
  // $args['post_status'] = 'publish';
  $q = new WP_Query($args);
  if( $q->have_posts() ){
    while($q->have_posts()): $q->the_post();
    	echo '<div class="three-col__item">';
get_template_part( 'template-parts/blog-item', get_post_format() );
	echo '</div>';
    endwhile;
    wp_reset_postdata();

   } else {

    get_template_part( 'template-parts/content', 'none' );

  }
  die();
}
 
 
add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');



function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');





function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyCPr5HMalv0zV6XqvZOzmSFnW12IKVsC68';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');



// send to mail
add_action( 'wp_ajax_nopriv_send_form', 'send_form' );
add_action( 'wp_ajax_send_form', 'send_form' );

function send_form(){

	$subject  = isset($_POST['data']['subject']) && !empty($_POST['data']['subject'])? $_POST['data']['subject'] :'';
	$emailTo  = isset($_POST['data']['emailTo']) && !empty($_POST['data']['emailTo'])? $_POST['data']['emailTo'] :'';

/******************************************************************************************************************************************/

	$name  = isset($_POST['data']['name']) && !empty($_POST['data']['name'])? 'Company name: '.$_POST['data']['name'].'<br>':'';
	$email  = isset($_POST['data']['email']) && !empty($_POST['data']['email'])? 'Email: '.$_POST['data']['email'].'<br>':'';
	$address  = isset($_POST['data']['address']) && !empty($_POST['data']['address'])? 'Address: '.$_POST['data']['address'].'<br>':'';
	$zip  = isset($_POST['data']['zip']) && !empty($_POST['data']['zip'])? 'ZIP: '.$_POST['data']['zip'].'<br>':'';
	$city  = isset($_POST['data']['city']) && !empty($_POST['data']['city'])? 'City: '.$_POST['data']['city'].'<br>':'';
	$remarks  = isset($_POST['data']['remarks']) && !empty($_POST['data']['remarks'])? 'Remarks: '.$_POST['data']['remarks'].'<br>':'';

/******************************************************************************************************************************************/
/******************************************************************************************************************************************/

	$country  = isset($_POST['data']['country']) && !empty($_POST['data']['country'])? 'Country: '.$_POST['data']['country'].'<br>':'';
	$departure  = isset($_POST['data']['departure']) && !empty($_POST['data']['departure'])? 'Departure: '.$_POST['data']['departure'].'<br>':'';
	$destination  = isset($_POST['data']['destination']) && !empty($_POST['data']['destination'])? 'Destination: '.$_POST['data']['destination'].'<br>':'';
	// $email  = isset($_POST['data']['email']) && !empty($_POST['data']['email'])? 'Email: '.$_POST['data']['email'].'<br>':'';
	$info  = isset($_POST['data']['info']) && !empty($_POST['data']['info'])? 'Info: '.$_POST['data']['info'].'<br>':'';
	// $name  = isset($_POST['data']['name']) && !empty($_POST['data']['name'])? 'Name: '.$_POST['data']['name'].'<br>':'';
	$packing  = isset($_POST['data']['packing']) && !empty($_POST['data']['packing'])? 'Packing: '.$_POST['data']['packing'].'<br>':'';
	$phone  = isset($_POST['data']['phone']) && !empty($_POST['data']['phone'])? 'Phone: '.$_POST['data']['phone'].'<br>':'';
	$procedure  = isset($_POST['data']['procedure']) && !empty($_POST['data']['procedure'])? 'Procedure: '.$_POST['data']['procedure'].'<br>':'';
	$quantity  = isset($_POST['data']['quantity']) && !empty($_POST['data']['quantity'])? 'Quantity: '.$_POST['data']['quantity'].'<br>':'';
	$weight  = isset($_POST['data']['weight']) && !empty($_POST['data']['weight'])? 'Weight: '.$_POST['data']['weight'].'<br>':'';
	$сommodity  = isset($_POST['data']['сommodity']) && !empty($_POST['data']['сommodity'])? 'Сommodity: '.$_POST['data']['сommodity'].'<br>':'';


/******************************************************************************************************************************************/
/******************************************************************************************************************************************/

	// $procedure  = isset($_POST['data']['procedure']) && !empty($_POST['data']['procedure'])? 'Procedure: '.$_POST['data']['procedure'].'<br>':'';
	$commodityCode  = isset($_POST['data']['commodityCode']) && !empty($_POST['data']['commodityCode'])? 'CommodityCode: '.$_POST['data']['commodityCode'].'<br>':'';
	$commodity  = isset($_POST['data']['commodity']) && !empty($_POST['data']['commodity'])? 'Commodity: '.$_POST['data']['commodity'].'<br>':'';
	$origin  = isset($_POST['data']['origin']) && !empty($_POST['data']['origin'])? 'Origin: '.$_POST['data']['origin'].'<br>':'';


  $body="

	$name
	$email
	$address
	$zip
	$city
	$remarks

	$country
	$departure
	$destination
	$info
	$packing
	$phone
	$procedure
	$quantity
	$weight
	$сommodity

	$commodityCode
	$commodity
	$origin

  ";
  
  $headers = "From: ab-com <wordpress@ab-com.loc>\r\nContent-type: text/html; charset=utf-8 \r\n";
  $result = wp_mail( $emailTo, $subject, $body, $headers);
  if ($result) {
    echo "success";
  } else {
    echo "error";
  }

  exit();
}


// add_action( 'wp_ajax_nopriv_application_form', 'application_form' );
// add_action( 'wp_ajax_application_form', 'application_form' );

// function application_form(){

// $emailTo  = isset($_POST['data']['emailTo']) && !empty($_POST['data']['emailTo'])? $_POST['data']['emailTo'] :'';
// $subject  = isset($_POST['data']['subject']) && !empty($_POST['data']['subject'])? $_POST['data']['subject'] :'';


// $country  = isset($_POST['data']['country']) && !empty($_POST['data']['country'])? 'Country: '.$_POST['data']['country'].'<br>':'';
// $departure  = isset($_POST['data']['departure']) && !empty($_POST['data']['departure'])? 'Departure: '.$_POST['data']['departure'].'<br>':'';
// $destination  = isset($_POST['data']['destination']) && !empty($_POST['data']['destination'])? 'Destination: '.$_POST['data']['destination'].'<br>':'';
// $email  = isset($_POST['data']['email']) && !empty($_POST['data']['email'])? 'Email: '.$_POST['data']['email'].'<br>':'';
// $info  = isset($_POST['data']['info']) && !empty($_POST['data']['info'])? 'Info: '.$_POST['data']['info'].'<br>':'';
// $name  = isset($_POST['data']['name']) && !empty($_POST['data']['name'])? 'Name: '.$_POST['data']['name'].'<br>':'';
// $packing  = isset($_POST['data']['packing']) && !empty($_POST['data']['packing'])? 'Packing: '.$_POST['data']['packing'].'<br>':'';
// $phone  = isset($_POST['data']['phone']) && !empty($_POST['data']['phone'])? 'Phone: '.$_POST['data']['phone'].'<br>':'';
// $procedure  = isset($_POST['data']['procedure']) && !empty($_POST['data']['procedure'])? 'Procedure: '.$_POST['data']['procedure'].'<br>':'';
// $quantity  = isset($_POST['data']['quantity']) && !empty($_POST['data']['quantity'])? 'Quantity: '.$_POST['data']['quantity'].'<br>':'';
// $weight  = isset($_POST['data']['weight']) && !empty($_POST['data']['weight'])? 'Weight: '.$_POST['data']['weight'].'<br>':'';
// $сommodity  = isset($_POST['data']['сommodity']) && !empty($_POST['data']['сommodity'])? 'Сommodity: '.$_POST['data']['сommodity'].'<br>':'';


//   $body="
// 	$country
// 	$departure
// 	$destination
// 	$email
// 	$info
// 	$name
// 	$packing
// 	$phone
// 	$procedure
// 	$quantity
// 	$weight
// 	$сommodity
//   ";
//   $headers = "From: ab-com <wordpress@ab-com.loc>\r\nContent-type: text/html; charset=utf-8 \r\n";
//   $result = wp_mail( $emailTo, $subject, $body, $headers);
//   if ($result) {
//     echo "success";
//   } else {
//     echo "error";
//   }

//   exit();
// }



pll_register_string('request_form', 'requestform'); 
pll_register_string('show_more', 'showmore'); 